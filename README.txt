Drupal module: Commerce Checkout Combine Status Messages
========================================================

This module removes the inline status messages on the Drupal Commerce checkout
pages and combines them above the form.

INSTALLATION
======================

- Enable module et voila!
