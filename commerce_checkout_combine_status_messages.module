<?php
/**
 * @file
 * Commerce Checkout Combine Status Messages module.
 *
 * Removes the inline status messages on the Drupal Commerce checkout pages
 * and combines them above the form.
 */

/**
 * Implements hook_form_alter().
 */
function commerce_checkout_combine_status_messages_form_alter(&$form, &$form_state, $form_id) {
  if (strpos($form_id, 'commerce_checkout_form_') === 0) {
    $checkout_page_id = substr($form_id, 23);
    // Find all panes for our current checkout page and remove the inline
    // messages.
    foreach (commerce_checkout_panes(array(
      'enabled' => TRUE,
      'page' => $checkout_page_id,
    )) as $pane_id => $checkout_pane) {
      if (isset($form[$pane_id][$pane_id . '_messages'])) {
        unset($form[$pane_id][$pane_id . '_messages']);
      }
    }

    if (!empty($form_state['storage']['messages'])) {
      $status_messages = array();
      foreach ($form_state['storage']['messages'] as $pane_messages) {
        foreach ($pane_messages as $status => $messages) {
          foreach ($messages as $message) {
            $status_messages[$status][] = $message;
          }
        }
      }

      if (!empty($status_messages)) {
        foreach ($status_messages as $status => $messages_per_status) {
          foreach ($messages_per_status as $message) {
            // Add the messages to the session again.
            drupal_set_message($message, $status);
          }
        }

        // Because we previously added the messages back to the session,
        // We can print the messages using theme('status_messages').
        $form['error_message']['#markup'] = theme('status_messages');
      }
    }
  }
}
